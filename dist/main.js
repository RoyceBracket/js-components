'use strict';
var ba = {};

(function () {

	ba = {
		PredicateFactory: PredicateFactory,
		MessageBus: MessageBus,
		ExcelBuilder: ExcelBuilder,
		ClassFactory: ClassFactory

		/*SEE BOTTOM OF PAGE FOR VARS REFFERENCES*/
	};

	var Utils = function () {
		var service = {
			isObject: isObject,
			isArray: isArray,
			isNumber: isNumber,
			isNull: isNull,
			isUndefined: isUndefined,
			isString: isString,
			isEmpty: isEmpty,

			camelize: camelize,
			hasKeys: hasKeys,
			clearUndefineds: clearUndefineds,
			clone: clone,
			undefinedToEmptyObject: undefinedToEmptyObject
		};

		return service;

		function isObject(obj) {
			return !isArray(obj) && !isNumber(obj) && !isString(obj) && !isNull(obj) && !isUndefined(obj) && obj;
		}

		function isArray(obj) {
			return Array.isArray(obj);
		}

		function isNumber(obj) {
			return !isNaN(obj);
		}

		function isNull(obj) {
			return obj === null;
		}

		function isUndefined(obj) {
			return obj === undefined;
		}

		function isString(obj) {
			return typeof obj === 'string';
		}

		function hasKeys(obj) {
			for (var prop in obj)
				if (obj.hasOwnProperty(prop))
					return true;

			return false;
		}

		function isEmpty(obj) {
			// handle arrays
			if (isArray(obj))
				return obj.length > 0;

			// handle nulls, undefineds & zeros
			if (isUndefined(obj) || isNull(obj) || !obj)
				return true;

			// handle numbers
			if (isNumber(obj))
				return false;

			// handle strings
			if (isString(obj))
				return obj.length > 0;

			// finally, handle objects
			return !hasKeys(obj);
		}

		function isDeepEmpty(obj) {
			var deepFlag = true;

			if (!isObject(obj))
				throw Error('isDeepEmpty arguments must be an object');

			if (isEmpty(obj))
				return true;

			for (var prop in obj) {
				if ((isNumber(obj[prop]) || isString(obj[prop]) || isArray(obj[prop]) && obj[prop]))
					return false;
				else if (isObject(obj[prop]))
					deepFlag = isDeepEmpty(obj[prop]);
				if (!deepFlag)
					return false;
			}

			return deepFlag;
		}

		function clearUndefineds(obj) {
			if (!isObject(obj))
				throw Error('clearUndefineds arguments must be an object');

			for (var prop in obj) {
				if (isUndefined(obj[prop]))
					delete obj[prop];
				if (isObject(obj[prop]) && isDeepEmpty(obj[prop]))
					delete obj[prop];
				if (isObject(obj[prop]))
					clearUndefineds(obj[prop]);
			}
		}

		function clone(obj) {
			if (obj === null || typeof obj !== 'object')
				return obj;

			var copy = obj.constructor();

			for (var attr in obj)
				if (obj.hasOwnProperty(attr))
					copy[attr] = obj[attr];

			return copy;
		}

		function undefinedToEmptyObject(obj) {
			return obj || {};
		}

		function camelize(str) {
			return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
				if (+match === 0) return ''; // or if (/\s+/.test(match)) for white spaces
				return index == 0 ? match.toLowerCase() : match.toUpperCase();
			});
		}
	}();

	/** ClassFactory */
	function ClassFactory(defData) {
		var service = {
			create: create(defData)
		};

		return service;

		/** PUBLIC */
		function create(defData) {
			baClass.prototype = {
				toDefault: toDefault,
				toOrigin: toOrigin,
				toModified: toModified
			}

			return baClass;

			function baClass(data, allowPrivates) {
				var self = this;

				self.__privates = {
					_data: data,
					_defData: defData,
					_modified: {},
				};

				classify(self.__privates._modified, self.__privates._data, self.__privates._defData, self);

				return self;
			}

			function toDefault() {
				var self = this;
				var data = Utils.clone(self.__privates._defData);

				return data;
			}

			function toOrigin() {
				var self = this;
				var data = Utils.clone(self.__privates._data);

				return data;
			}

			function toModified(clearDefaults) {
				var self = this;
				var data = Utils.clone(self.__privates._modified);

				if (clearDefaults)
					Utils.clearUndefineds(data);

				return data;
			}
		}

		/** PRIVATE */
		function classify(modifiedData, data, defData, classifyData) {
			data = Utils.undefinedToEmptyObject(data);
			classifyData = Utils.undefinedToEmptyObject(classifyData);

			for (var prop in defData)
				if (defData.hasOwnProperty(prop))
					if (Utils.isObject(defData[prop])) {
						modifiedData[prop] = {};
						classifyData[prop] = classify(modifiedData[prop], data[prop], defData[prop], classifyData[prop]);
						Object.defineProperty(classifyData[prop], 'toDefault', { value: defData[prop] });
						Object.defineProperty(classifyData[prop], 'toOrigin', { value: data[prop] });
					}
					else {
						modifiedData[prop] = undefined;
						Object.defineProperty(classifyData, prop, {
							// handlers
							get: get(prop, modifiedData),
							set: set(prop, modifiedData),
							enumerable: true
						});
					}

			return classifyData;

			function get(prop, modifiedData) {
				return function _get() {
					var modifiedEmpty = modifiedData[prop] ? Object.keys(modifiedData[prop]).length === 0 && modifiedData[prop].constructor === Object : true;

					if (modifiedEmpty) {
						if (Utils.isArray(data[prop]))
							modifiedData[prop] = data[prop].slice(0);
						else if (Utils.isArray(defData[prop]))
							modifiedData[prop] = defData[prop].slice(0);
					}

					var isUndefined = Utils.isUndefined;
					return !isUndefined(modifiedData[prop]) ? modifiedData[prop] :
						!isUndefined(data[prop]) ? data[prop] :
							defData[prop];
				}
			}

			function set(prop, modifiedData) {
				return function (newVal) {
					modifiedData[prop] = newVal;
				}
			}
		}
	}

	/** Predicate Factory
	 *
	 * API
	 * {Function} where
	 * {Function} query
	 */
	function PredicateFactory() {
		var service = {
			where: where,
			query: query
		};

		/** PUBLIC */
		/**
		 * Where
		 *
		 * Used with Array.prototype.find. example: contacts.find(where('id').equals(6))
		 *
		 * @param {String} propPath		path to prop on obj. example : 'customer.id'
		 *
		 * @return {Operator}
		 */
		function where(propPath) {
			var path = propPath ?
				propPath.split('.') : [];

			return new Operator(path);
		}

		/**
		 * Query
		 *
		 * Used to composite AND & OR queries. if provided with an array of Operators the array will act as AND query,
		 * If provided with an array of Operators arrays, each array will act as AND query and between the arrays an OR query will be executed.
		 *
		 * @param {Array} arr		can be either an array of Operators or an array of Operators Arrays
		 *
		 * @return {Function}
		 */
		function query(arr) {
			return function (obj) {
				var action = Utils.isArray(arr[0]) ?
					_queryOperatorsArrays : _queryOperatorsArray;

				return action(arr, obj);
			}
		}

		/** PRIVATE */

		/**
		 * Query Operators Arrays
		 *
		 * Itirate through operatorsArray to determine _successFlag value.
		 * _successFlag acts as OR (||) indicator.
		 *
		 * @param {Array} 	operatorsArrays		an array of Operators arrays to run against the OR _successFlag check
		 * @param {Object} 	obj								an object on which each action in the array will be checked against
		 */
		function _queryOperatorsArrays(operatorsArrays, obj) {
			var _successFlag = false;

			operatorsArrays.forEach(function (operatorArray) {
				if (_queryOperatorsArray(operatorArray, obj)) {
					_successFlag = true;
					return;
				}
			});

			return _successFlag;
		}

		/**
		 * Query Operators Array
		 *
		 * Itirate through operatorsArray to determine _successFlag value.
		 * _successFlag acts as AND (&&) indicator.
		 *
		 * @param {Array} 	operatorsArray		an array of Operators to run against the AND _successFlag check
		 * @param {Object} 	obj								an object on which each action in the array will be checked against
		 */
		function _queryOperatorsArray(operatorsArray, obj) {
			var _successFlag = true

			if (!Utils.isArray(operatorsArray))
				return;

			operatorsArray.forEach(function (queryItem) {
				if (!queryItem(obj)) {
					_successFlag = false;
					return;
				}

				return _successFlag;

			});
		}

		/**
		 * Deep Get
		 *
		 * Get property from object
		 *
		 * @param {Object} obj			source object
		 * @param {Array}  propArr	array the defines the path of the prop
		 */
		function _deepGet(obj, propArr) {
			var arrCopy = propArr.slice();
			var propName = arrCopy.shift();

			if (arrCopy.length)
				return _deepGet(obj[propName], arrCopy);
			else
				return obj[propName];
		}

		/**
		 * Operate
		 *
		 * Operate an action ageinst specified property and value
		 *
		 * @param {Function} action	action to perform
		 *
		 * @return {Function}
		 */
		function $operate(action) {
			var self = this;
			var propPath = self.propPath;

			return function (obj) {
				var objVal = propPath.length ?
					_deepGet(obj, propPath) :
					obj;

				return action(objVal, obj);
			}
		}

		/**
		 * Operator
		 *
		 * Reveals api for operators functionality
		 *
		 * @param {String} propPath		path to prop on obj. example : 'customer.id'
		 */
		function Operator(propPath) {
			var self = this;

			self.propPath = propPath;
			self.action = [];
		}

		function equals(value) {
			var self = this;

			return self.$operate(function (objVal) { return objVal === value; });
		}

		function notEquals(value) {
			var self = this;

			return self.$operate(function (objVal) { return objVal !== value; });
		}

		function greaterThan(value) {
			var self = this;

			return self.$operate(function (objVal) { return objVal > value; });
		}

		function greaterThanEquals(value) {
			var self = this;

			return self.$operate(function (objVal) { return objVal >= value; });
		}

		function lessThan(value) {
			var self = this;

			return self.$operate(function (objVal) { return objVal < value; });
		}

		function lessThanEquals(value) {
			var self = this;

			return self.$operate(function (objVal) { return objVal <= value; });
		}

		function has(value) {
			var self = this;

			return self.$operate(function (objVal) {
				return objVal ? objVal.indexOf(value) !== -1 : false;
			});
		}

		function hasNot(value) {
			var self = this;

			return self.$operate(function (objVal) {
				return objVal ? objVal.indexOf(value) === -1 : false;
			});
		}

		function executeWith() {
			var self = this;
			var args = arguments;

			return self.$operate(function (objFunc, obj) {
				try {
					objFunc.apply(obj, args);
					return true;
				} catch (error) {
					console.log(error);
					return false;
				}
			});
		}

		function compare(isAscending) {
			var self = this;
			var propPath = self.propPath;

			return function (objA, objB) {
				var valA = _deepGet(objA, propPath);
				var valB = _deepGet(objB, propPath);

				var factor = isAscending ? 1 : -1;

				var res = valA > valB ? 1 :
					valA < valB ? -1 :
						0;

				return res * factor;
			}
		}

		function val() {
			var self = this;

			return self.$operate(function (objVal) {
				return objVal;
			});
		}

		Operator.prototype = {
			$operate: $operate,

			eq: equals,
			notEq: notEquals,
			gt: greaterThan,
			gte: greaterThanEquals,
			lt: lessThan,
			lte: lessThanEquals,
			has: has,
			hasNot: hasNot,
			executeWith: executeWith,
			compare: compare,
			val: val
		};

		return service;
	}

	/** Message Bus
	 *
	 * API
	 * {Function} on
	 * {Function} emit
	 */
	function MessageBus() {
		var _messages = {};

		var service = {
			on: on,
			emit: emit
		};

		/**
		 * On
		 *
		 * Register callback to an event
		 *
		 * @param {String} 		event			event name
		 * @param {Function} 	cb				cb to executed
		 *
		 * @return {Function} destroy		removes the cb from the event
		 */
		function on(event, cb) {
			_messages[event] = _messages[event] || [];
			_messages[event].push(cb);

			return destroyer(event, cb);
		}

		/**
		 * Emit
		 *
		 * Calls an event
		 *
		 * @param {String}	event event name to call
		 */
		function emit(event) {
			Array.prototype.shift.apply(arguments);
			var args = arguments;

			_emit(event, args);

			function _emit(event, args) {
				if (!_messages[event])
					return;

				_messages[event].forEach(function (cb) {
					cb.apply(this, args);
				});
			}
		}

		/**
		 * Destroyer
		 *
		 * Initiate destory function for specific event and cb
		 *
		 * @param {String} 		event			event from which we remove callback
		 * @param {Function} 	cb				callback to remove
		 *
		 * @return {Function} destory		removes the cb from the event
		 */
		function destroyer(event, cb) {
			return function destroy() {
				var cbIndex = _messages[event].indexOf(cb);

				_messages[event].splice(cbIndex, 1);
			}
		}

		return service;
	}

	/** Excel Builder
	 *
	 * API
	 * {Function} createFile
	 */
	function ExcelBuilder() {
		var ExcelFile = function (fileName) {
			var self = this;
			self.content = '';
			self.fileName = fileName || '';
		}

		var service = {
			createFile: ExcelFile
		};

		ExcelFile.prototype = {
			enter: enter,
			tab: tab,
			write: write,
			writeStr: writeStr,
			download: download,
			setFileName: setFileName
		};

		/**
		 * Enter
		 *
		 * Move to the next line
		 *
		 * @param {Number} repeat
		 *
		 * @return {ExcelFile} self
		 */
		function enter(repeat) {
			var self = this;

			_repeatOn(function () {
				self.content += '\n';
			}, repeat || 1);

			return self;
		}

		/**
		 * Tab
		 *
		 * Move to the next cell
		 *
		 * @param {Number} repeat
		 *
		 * @return {ExcelFile} self
		 */
		function tab(repeat) {
			var self = this;

			_repeatOn(function () {
				self.content += ',';
			}, repeat || 1);

			return self;
		}

		/**
		 * Write String
		 *
		 * @param {String} str string to write
		 *
		 * @return {ExcelFile} self
		 */
		function writeStr(str) {
			var self = this;
			self.content += '"' + str + '"';
			return self;
		}

		/**
		 * Set File name
		 *
		 * @param {String} fileName
		 *
		 * @return {ExcelFile} self
		 */
		function setFileName(fileName) {
			var self = this;
			self.fileName = fileName;
			return self;
		}

		/** Write
		 *
		 * Write data in to file, Making tab after writing values and enter after writing arrays
		 *
		 * @param 			 data 				can be value (string or number), array of values, or array of arrays of values.
		 * @param {Bool} cancelEnter	indicates if enter after array end should be canceld.
		 */
		function write(data, cancelEnter) {
			var self = this;

			/** if data is string or number we just write it to the file and tab */
			if (typeof (data) === 'string' || !isNaN(data)) {
				self.writeStr(data).tab();
				return self;
			}

			/** if data is array we go here */
			if (Utils.isArray(data)) {
				for (var i = 0, iLength = data.length; i < iLength; i++) {
					var innerData = data[i];

					self.write(innerData, cancelEnter);
				}

				if (!cancelEnter)
					self.enter();
			}
			return self;
			/** if data is not a string or an array we throw an error */
		}

		/**
		 * Download
		 *
		 * Download file
		 */
		function download() {
			var self = this;
			var content = self.content;
			var fileName = self.fileName + '.csv';

			var a = document.createElement('a');

			document.body.appendChild(a);
			a.style = 'display: none';

			var blob = new Blob([content], { type: 'text/plain' });
			var url = window.URL.createObjectURL(blob);

			a.href = url;
			a.download = fileName;
			a.click();
			a.remove();
			window.URL.revokeObjectURL(url);
		}

		function _repeatOn(cb, amount) {
			for (var i = 0; i < amount; i++) {
				cb();
			}
		}

		return service;
	}

	//extends ba with vars
	ba.Utils = Utils;
})();